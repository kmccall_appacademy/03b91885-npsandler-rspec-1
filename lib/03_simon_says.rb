def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, times = 1)
  string = word  + ' ' + word
  i = 2
    while i < times
      string = string + ' ' + word
      i += 1
    end
  string
end

def start_of_word(word, number = 1)
  word[0...number]
end

def first_word(string)
  string.split.first
end

def titleize(string)
    little_words = ["and", "or", "the", "to", "the", "a", "but", "over"]
    array = string.split(" ").map do |word|
        if little_words.include?(word)
            word
        else
            word.capitalize
        end
    end
    array[0].capitalize!
    array.join(' ')
end
