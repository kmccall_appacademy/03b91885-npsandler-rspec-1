def ftoc(far)
  return 0 if far == 32
  (far.to_f - 32) * 5 / 9
end

def ctof(cel)
  return 32 if cel == 0
  (cel.to_f * 9 / 5) + 32
end
