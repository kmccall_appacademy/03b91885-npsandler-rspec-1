def translate(string)
  array = string.split
  array.map! { |word| latinize(word)}
  array.join(' ')

end

def latinize(word)
  alpha = ('a'..'z').to_a
  vowels = %w[a e i o u]
  consonants = alpha - vowels

  if   vowels.include?(word[0..0])
    word + 'ay'
  elsif word[0..2] == "sch"
    word[3..word.length]+"schay"
  elsif word[0..2] == "squ"
    word[3..word.length]+"squay"
 elsif word[0..1] == "qu"
    word[2..word.length]+"quay"
  elsif consonants.include?(word[0..0]) && consonants.include?(word[1..1]) && consonants.include?(word[2..2])
    word[3..-1] + word[0..2] + 'ay'
  elsif consonants.include?(word[0..0]) && consonants.include?(word[1..1])
    word[2..-1] + word[0..1] + 'ay'
  elsif consonants.include?(word[0])
    word[1..-1] + word[0..0] + 'ay'
  end
end
