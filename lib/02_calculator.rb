def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array)
  array.reduce(0) { |acc, el| acc + el }
end

p add(15, 3)
p subtract(15, 3)
p sum([1, 2, 3, 4, 5, 6, 7, 8, 9, ])
